# Adapted from Ed's Vertex
FROM ubuntu:14.04
MAINTAINER Ben Kovach <bkovach5@uga.edu>

# Set environment to non-interactive
ENV DEBIAN_FRONTEND noninteractive

# Update the Ubuntu image
RUN apt-get update
RUN apt-get install -y software-properties-common python-software-properties
RUN add-apt-repository ppa:chris-lea/node.js
RUN add-apt-repository -y ppa:nginx/stable
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ precise universe" >> /etc/apt/sources.list
RUN apt-get update

## TOOLS
RUN apt-get install -y vim wget git curl

## NODE.JS
RUN apt-get install -y nodejs

## ELASTICSEARCH
RUN cd ~
RUN wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.2.2.deb
RUN sudo dpkg -i elasticsearch-1.2.2.deb
# RUN /etc/init.d/elasticsearch # start es

## JAVA
RUN sudo apt-get -y install openjdk-7-jdk

## START ES
CMD /etc/init.d/elasticsearch